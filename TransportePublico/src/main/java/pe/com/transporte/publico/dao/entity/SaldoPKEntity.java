/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.com.transporte.publico.dao.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Entity;

/**
 *
 * @author Jomimeox
 */
@Data
@EqualsAndHashCode
@ToString
@Embeddable
public class SaldoPKEntity implements Serializable {

    @Basic(optional = false)
    @Column(name = "IdCliente")
    private int idCliente;
    @Basic(optional = false)
    @Column(name = "IdTarjeta")
    private int idTarjeta;

    public SaldoPKEntity() {
    }

    public SaldoPKEntity(int idCliente, int idTarjeta) {
        this.idCliente = idCliente;
        this.idTarjeta = idTarjeta;
    }
    
}
