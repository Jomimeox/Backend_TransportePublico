package pe.com.transporte.publico.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
public class AsociarTarjetaRequest implements Serializable {

    private Integer identificador;
    private List<Tarjeta> asociarTarjetas;

}
