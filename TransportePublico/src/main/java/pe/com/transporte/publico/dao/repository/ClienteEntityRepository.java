package pe.com.transporte.publico.dao.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pe.com.transporte.publico.dao.entity.ClienteEntity;

@Repository
public interface ClienteEntityRepository extends JpaRepository<ClienteEntity, Integer> {

    ClienteEntity findByCorreoAndContrasenaAndEstado(String correo, String clave, Character estado);
    ClienteEntity findByCorreo(String correo);
    ClienteEntity findByIdCliente(Integer identificador);

}
