package pe.com.transporte.publico.dto;

import lombok.Data;

@Data
public class ActualizarClaveRequest {

    private Integer identificador;
    private String claveAntigua;
    private String claveNueva;
}
