package pe.com.transporte.publico.dto;

import lombok.Data;

@Data
public class ActualizarClaveResponse {

    private Audit audit;
}
