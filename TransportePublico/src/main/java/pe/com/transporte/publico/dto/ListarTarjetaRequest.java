package pe.com.transporte.publico.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ListarTarjetaRequest {
    private Integer identificador;
}
