package pe.com.transporte.publico.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class LoginResponse {

    private Audit audit;
    private Integer identificador;
    private String nombre;
    private String correo;
    private String fechaRegistro;
}
