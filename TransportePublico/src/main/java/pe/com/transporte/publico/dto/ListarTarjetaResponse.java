package pe.com.transporte.publico.dto;

import lombok.Data;

import java.util.List;

@Data
public class ListarTarjetaResponse {

    private Audit audit;
    private List<TarjetaAsociada> listaTarjetas;

}
