/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.com.transporte.publico.dao.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jomimeox
 */
@Entity
@Data
@EqualsAndHashCode
@ToString
@Table(name = "cliente_tarjeta")
public class ClienteTarjetaEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ClienteTarjetaPKEntity clienteTarjetaPK;
    @Column(name = "FechaRegistro")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaRegistro;
    @Column(name = "Estado")
    private Character estado;
    @JoinColumn(name = "IdCliente", referencedColumnName = "IdCliente", insertable = false, updatable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private ClienteEntity cliente;
    @JoinColumn(name = "IdTarjeta", referencedColumnName = "IdTarjeta", insertable = false, updatable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private TarjetaEntity tarjeta;
    
}
