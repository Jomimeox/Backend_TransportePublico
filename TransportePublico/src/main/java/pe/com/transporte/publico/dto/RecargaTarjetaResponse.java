package pe.com.transporte.publico.dto;

import lombok.Data;

@Data
public class RecargaTarjetaResponse {

    private Audit audit;
    private String numeroTransaccion;
    private String fechaTransaccion;

}
