package pe.com.transporte.publico.dto;

import lombok.Data;

@Data
public class RegistrarUsuarioRequest {
    private String usuario;
    private String correo;
    private Integer dni;
    private Integer numeroCelular;
    private String clave;
}
