/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.com.transporte.publico.dao.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Jomimeox
 */
@Entity
@Data
@EqualsAndHashCode
@ToString
@Table(name = "tarjeta")
public class TarjetaEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idtarjeta")
    private Integer idTarjeta;
    @Column(name = "nrotarjeta")
    private String nroTarjeta;
    @Column(name = "fechaexpiracion")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaExpiracion;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "tarjeta", fetch = FetchType.LAZY)
    private Collection<ClienteTarjetaEntity> clienteTarjetaCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "tarjeta", fetch = FetchType.LAZY)
    private Collection<DetalleMovimientoEntity> detalleMovimientoCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "tarjeta", fetch = FetchType.LAZY)
    private Collection<SaldoEntity> saldoCollection;
    @JoinColumn(name = "idtiposervicio", referencedColumnName = "idtiposervicio")
    @ManyToOne(fetch = FetchType.LAZY)
    private TiposervicioEntity idTipoServicio;
    
}
