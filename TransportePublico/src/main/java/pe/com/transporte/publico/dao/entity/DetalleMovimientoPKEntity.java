/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.com.transporte.publico.dao.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author Jomimeox
 */
@Embeddable
@Data
@EqualsAndHashCode
@ToString
public class DetalleMovimientoPKEntity implements Serializable {

    @Basic(optional = false)
    @Column(name = "IdCliente")
    private int idCliente;
    @Basic(optional = false)
    @Column(name = "IdTarjeta")
    private int idTarjeta;
    @Basic(optional = false)
    @Column(name = "NroTransaccion")
    private int nroTransaccion;

    public DetalleMovimientoPKEntity() {
    }

    public DetalleMovimientoPKEntity(int idCliente, int idTarjeta, int nroTransaccion) {
        this.idCliente = idCliente;
        this.idTarjeta = idTarjeta;
        this.nroTransaccion = nroTransaccion;
    }

    
}
