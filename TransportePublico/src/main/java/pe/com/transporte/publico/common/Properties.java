package pe.com.transporte.publico.common;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
@Data
public class Properties {

    @Value("${plantilla.html.pago.exitoso}")
    private String plantillaHtml;

    @Value("${plantilla.html.recuperar.clave}")
    private String plantillaHtmlRecuperarClave;
}
