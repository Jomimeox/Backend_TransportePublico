package pe.com.transporte.publico.service.mapper;

import org.mapstruct.*;
import org.mapstruct.factory.Mappers;
import pe.com.transporte.publico.common.EstadoEnum;
import pe.com.transporte.publico.dao.entity.TarjetaEntityDto;
import pe.com.transporte.publico.dto.TarjetaAsociada;

@Mapper
public abstract class TarjetaMapper {

    public static TarjetaMapper INSTANCE = Mappers.getMapper(TarjetaMapper.class);

    @Mappings({
            @Mapping(source = "request.IDTARJETA", target = "idTarjeta"),
            @Mapping(source = "request.NROTARJETA", target = "numeroTarjeta"),
            @Mapping(source = "request.DESCRIPCION", target = "tipoServicio"),
            @Mapping(source = "request.MONTO", target = "saldo"),
            @Mapping(source = "request.FECHAEXPIRACION", target = "FechaVencimiento")
    })
    public abstract TarjetaAsociada map(TarjetaEntityDto request);

    @AfterMapping
    protected void updateEstado(TarjetaEntityDto request, @MappingTarget TarjetaAsociada target){
        target.setEstadoTarjeta(EstadoEnum.valueOf(request.getESTADO()).getEstadoDescripcion());
    }

}
