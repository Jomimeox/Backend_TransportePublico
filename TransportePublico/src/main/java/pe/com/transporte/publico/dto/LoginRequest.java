package pe.com.transporte.publico.dto;

import lombok.Data;

@Data
public class LoginRequest {
    private String usuario;
    private String clave;
}
