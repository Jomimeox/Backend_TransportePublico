package pe.com.transporte.publico.dao.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pe.com.transporte.publico.dao.entity.TarjetaEntity;

@Repository
public interface TarjetaEntityRepository extends JpaRepository<TarjetaEntity, Integer> {
}
