package pe.com.transporte.publico.dto;

import lombok.Data;

@Data
public class TarjetaAsociada {

    private Integer idTarjeta;
    private String numeroTarjeta;
    private String tipoServicio;
    private String estadoTarjeta;
    private String FechaVencimiento;
    private Double saldo;
}
