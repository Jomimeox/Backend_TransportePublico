package pe.com.transporte.publico.service;

import pe.com.transporte.publico.dto.*;

public interface TarjetaService {

    ListarTarjetaResponse obtenerTarjetas(Integer identificador) throws Exception;

    Integer asociarTarjetas(AsociarTarjetaRequest request) throws Exception;

    RecargaTarjetaResponse recargarTarjeta(RecargaTarjetaRequest request) throws Exception;

    LoginResponse validarUsuario(LoginRequest request) throws Exception;

    RegistrarUsuarioResponse registrarUsuario(RegistrarUsuarioRequest request) throws Exception;

    ActualizarClaveResponse actualizarClave(ActualizarClaveRequest request) throws Exception;

    RecuperarContraseñaResponse recuperarClave(RecuperarContraseñaRequest request) throws Exception;

}
