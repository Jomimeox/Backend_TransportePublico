package pe.com.transporte.publico.dto;

import lombok.Data;

@Data
public class AsociarTarjetaResponse {

    private Audit audit;
    private Integer codigoIdentificador;

}
