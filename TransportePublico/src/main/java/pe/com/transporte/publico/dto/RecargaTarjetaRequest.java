package pe.com.transporte.publico.dto;

import lombok.Data;

@Data
public class RecargaTarjetaRequest {

    private Integer identificadorCliente;
    private String numeroTarjetaTransporte;
    private Double montoRecarga;
    private String numeroTarjetaPago;
    private String fechaExpiracion;
    private String CCV;
    private String nombreTitular;
    private String correoElectronico;

}
