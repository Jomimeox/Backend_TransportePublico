/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.com.transporte.publico.dao.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Jomimeox
 */
@Entity
@Data
@EqualsAndHashCode
@ToString
@Table(name = "saldo")
public class SaldoEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected SaldoPKEntity saldoPK;
    @Column(name = "Monto")
    private Long monto;
    @JoinColumn(name = "IdCliente", referencedColumnName = "IdCliente", insertable = false, updatable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private ClienteEntity cliente;
    @JoinColumn(name = "IdTarjeta", referencedColumnName = "IdTarjeta", insertable = false, updatable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private TarjetaEntity tarjeta;
    
}
