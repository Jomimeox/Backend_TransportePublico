package pe.com.transporte.publico.dto;

import lombok.Data;
import java.io.Serializable;

@Data
public class Tarjeta implements Serializable {

    private String numeroTarjeta;
    private Integer tipoServicio;
    private String fechaVencimiento;

}
