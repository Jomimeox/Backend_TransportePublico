package pe.com.transporte.publico.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class RegistrarUsuarioResponse {

    private Audit audit;
    private Integer identificador;
}
