package pe.com.transporte.publico.dto;

import lombok.Data;

@Data
public class RecuperarContraseñaRequest {

    private String correo;
}
