package pe.com.transporte.publico.dao.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;

@Data
@Entity
public class TarjetaEntityDto {

    @Id
    private Integer IDTARJETA;
    private String NROTARJETA;
    private String DESCRIPCION;
    private String ESTADO;
    private Double MONTO;
    private String FECHAEXPIRACION;

}
