package pe.com.transporte.publico.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import pe.com.transporte.publico.dto.*;
import pe.com.transporte.publico.service.TarjetaService;


@RestController
@RequestMapping("/transporte/publico")
@Validated
public class Controller {
	
	@Autowired
	private TarjetaService tarjetaService;
	
	@RequestMapping(value = "/asociarTarjeta", method = RequestMethod.POST,produces = "application/json; charset=UTF-8")
	public ResponseEntity<AsociarTarjetaResponse> asociarTarjeta(@RequestHeader HttpHeaders httpHeaders,
																 @RequestBody AsociarTarjetaRequest request) throws Exception{
		Integer numeroCliente = null;
		AsociarTarjetaResponse response = new AsociarTarjetaResponse();
		Audit audit = new Audit();

		try {
			numeroCliente = tarjetaService.asociarTarjetas(request);
			audit.setCodigoRespuesta(0);
			audit.setMensajeRespuesta("Registro exitoso");
		} catch (Exception e){
			System.out.println("Error: "+e);
			audit.setCodigoRespuesta(1);
			audit.setMensajeRespuesta("Error interno");
		}

		response.setAudit(audit);
		response.setCodigoIdentificador(numeroCliente);

		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	@RequestMapping(value = "/listarTarjeta/{identificador}", method = RequestMethod.GET,produces = "application/json; charset=UTF-8")
	public ResponseEntity<ListarTarjetaResponse> listarTarjetas(@RequestHeader HttpHeaders httpHeaders,
																@PathVariable("identificador") Integer identificador) throws Exception{
		return new ResponseEntity<>(tarjetaService.obtenerTarjetas(identificador), HttpStatus.OK);
	}

	@RequestMapping(value = "/recargarTarjeta", method = RequestMethod.POST,produces = "application/json; charset=UTF-8")
	public ResponseEntity<RecargaTarjetaResponse> listarTarjetas(@RequestHeader HttpHeaders httpHeaders,
																@RequestBody RecargaTarjetaRequest request) throws Exception{
		return new ResponseEntity<>(tarjetaService.recargarTarjeta(request), HttpStatus.OK);
	}

	@RequestMapping(value = "/login", method = RequestMethod.POST,produces = "application/json; charset=UTF-8")
	public ResponseEntity<LoginResponse> listarTarjetas(@RequestHeader HttpHeaders httpHeaders,
																 @RequestBody LoginRequest request) throws Exception{
		return new ResponseEntity<>(tarjetaService.validarUsuario(request), HttpStatus.OK);
	}

	@RequestMapping(value = "/registrarUsuario", method = RequestMethod.POST,produces = "application/json; charset=UTF-8")
	public ResponseEntity<RegistrarUsuarioResponse> listarTarjetas(@RequestHeader HttpHeaders httpHeaders,
														@RequestBody RegistrarUsuarioRequest request) throws Exception{
		return new ResponseEntity<>(tarjetaService.registrarUsuario(request), HttpStatus.OK);
	}

	@RequestMapping(value = "/actualizarClave", method = RequestMethod.POST,produces = "application/json; charset=UTF-8")
	public ResponseEntity<ActualizarClaveResponse> listarTarjetas(@RequestHeader HttpHeaders httpHeaders,
																   @RequestBody ActualizarClaveRequest request) throws Exception{
		return new ResponseEntity<>(tarjetaService.actualizarClave(request), HttpStatus.OK);
	}

	@RequestMapping(value = "/recuperarClave", method = RequestMethod.POST,produces = "application/json; charset=UTF-8")
	public ResponseEntity<RecuperarContraseñaResponse> listarTarjetas(@RequestHeader HttpHeaders httpHeaders,
																  @RequestBody RecuperarContraseñaRequest request) throws Exception{
		return new ResponseEntity<>(tarjetaService.recuperarClave(request), HttpStatus.OK);
	}

}
