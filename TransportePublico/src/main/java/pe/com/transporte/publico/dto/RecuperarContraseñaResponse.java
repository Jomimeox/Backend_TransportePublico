package pe.com.transporte.publico.dto;

import lombok.Data;

@Data
public class RecuperarContraseñaResponse {

    private Audit audit;
}
