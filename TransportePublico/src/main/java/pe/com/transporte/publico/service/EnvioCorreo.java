package pe.com.transporte.publico.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import pe.com.transporte.publico.common.Properties;
import pe.com.transporte.publico.dto.RecargaTarjetaRequest;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.UnsupportedEncodingException;
import java.util.UUID;

@Service
public class EnvioCorreo {

    @Autowired
    private Properties properties;

    @Autowired
    private JavaMailSenderImpl sender = new JavaMailSenderImpl();

    public void sendPago(RecargaTarjetaRequest request, String fechaTransaccion, Integer numeroTransaccion) throws MessagingException, UnsupportedEncodingException {

        String uuid = UUID.randomUUID().toString().replace("-", "");
        uuid = uuid.substring(0,4);

        String tarjeta =  "";

        tarjeta = request.getNumeroTarjetaPago().substring(0,4);
        tarjeta = tarjeta + " **** **** ";
        tarjeta = tarjeta + request.getNumeroTarjetaPago().substring(request.getNumeroTarjetaPago().length() - 4, request.getNumeroTarjetaPago().length());

        String plantillaHtml = properties.getPlantillaHtml();
        plantillaHtml = plantillaHtml.replace("$TARJETA", tarjeta);
        plantillaHtml = plantillaHtml.replace("$FECHATRANSACCION",fechaTransaccion);
        plantillaHtml = plantillaHtml.replace("$OPERACION", numeroTransaccion.toString());
        plantillaHtml = plantillaHtml.replace("$MONTO", String.valueOf(request.getMontoRecarga()));
        plantillaHtml = plantillaHtml.replace("$AUTORIZACIONBANCARIA", uuid);

        MimeMessage message = sender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message, true);
        helper.setFrom("transportemasivolima@gmail.com","Transporte Masivo de Lima");
        helper.setTo(request.getCorreoElectronico());
        helper.setSubject("Recibo de recarga");
        helper.setText(plantillaHtml, true);
        sender.send(message);

    }

    public void sendRecuperarClave(String correo, String nombre, String clave) throws MessagingException, UnsupportedEncodingException {

        String plantillaHtml = properties.getPlantillaHtmlRecuperarClave();
        plantillaHtml = plantillaHtml.replace("$NOMBRE", nombre);
        plantillaHtml = plantillaHtml.replace("$CONTRASENA",clave);

        MimeMessage message = sender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message, true);
        helper.setFrom("transportemasivolima@gmail.com","Transporte Masivo de Lima");
        helper.setTo(correo);
        helper.setSubject("Recuperar contraseña");
        helper.setText(plantillaHtml, true);
        sender.send(message);
    }

}
