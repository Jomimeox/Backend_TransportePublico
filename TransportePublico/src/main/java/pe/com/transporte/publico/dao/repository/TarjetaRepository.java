package pe.com.transporte.publico.dao.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import pe.com.transporte.publico.dao.entity.TarjetaEntityDto;

import java.util.List;

@Repository
public interface TarjetaRepository extends JpaRepository<TarjetaEntityDto, Integer> {

    @Query(value = "SELECT t.IdTarjeta IdTarjeta, t.NroTarjeta NroTarjeta, ts.Descripcion Descripcion, ts.Estado Estado, " +
            "s.Monto Monto, DATE_FORMAT(t.FechaExpiracion, '%d/%m/%Y') FechaExpiracion FROM tarjeta t " +
            "inner join cliente_tarjeta ct on ct.IdTarjeta = t.IdTarjeta " +
            "inner join cliente c on c.IdCliente = ct.IdCliente " +
            "inner join  tiposervicio ts on ts.IdTipoServicio = t.IdTipoServicio " +
            "inner join saldo s on s.IdTarjeta = ct.IdTarjeta " +
            "where c.IdCliente = :idCliente", nativeQuery= true)
    List<TarjetaEntityDto> buscarTarjetasAsociadas(@Param("idCliente") Integer idCliente);
}
