package pe.com.transporte.publico.service;

import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pe.com.transporte.publico.common.EstadoEnum;
import pe.com.transporte.publico.common.Util;
import pe.com.transporte.publico.dao.entity.*;
import pe.com.transporte.publico.dao.repository.*;
import pe.com.transporte.publico.dto.*;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
@RequiredArgsConstructor
public class TarjetaServiceImpl implements TarjetaService {

    private final TarjetaRepository tarjetaRepository;
    private final ClienteTarjetaEntityRepository clienteTarjetaEntityRepository;
    private final ClienteEntityRepository clienteEntityRepository;
    private final TarjetaEntityRepository tarjetaEntityRepository;
    private final TipoServicioEntityRepository tipoServicioEntityRepository;

    private final EnvioCorreo envioCorreo;

    public static final String ACCOUNT_SID = "AC1b74c57781ebf47dfd127ba756f509ae";
    public static final String AUTH_TOKEN  = "d5163f568721153ec850d82c9daec35b";

    @Override
    public ListarTarjetaResponse obtenerTarjetas(Integer identificador) throws Exception {
        return getListaTarjeta(tarjetaRepository.buscarTarjetasAsociadas(identificador));
    }

    @Override
    @Transactional
    public Integer asociarTarjetas(AsociarTarjetaRequest request) throws Exception {

        List<TiposervicioEntity> listaServicios =tipoServicioEntityRepository.findByEstadoEquals('A');
        Integer identificador;

        if(request.getIdentificador() == null){
            ClienteEntity cliente = new ClienteEntity();
            cliente.setFechaRegistro(new Date());
            cliente.setEstado('A');
            cliente = clienteEntityRepository.save(cliente);
            identificador = cliente.getIdCliente();
        }else{
            identificador = request.getIdentificador();
        }

        List<TarjetaEntity> listaTarjetaEntity = new ArrayList<>();

        try {
            for (Tarjeta tarjetaRequest : request.getAsociarTarjetas()) {
                TarjetaEntity tarjeta = new TarjetaEntity();
                tarjeta.setNroTarjeta(tarjetaRequest.getNumeroTarjeta());

                for ( TiposervicioEntity servicio : listaServicios  ) {
                    if(servicio.getIdTipoServicio() == tarjetaRequest.getTipoServicio()){
                        tarjeta.setIdTipoServicio(servicio);
                    }
                }
                if(tarjetaRequest.getFechaVencimiento() != null){
                    tarjeta.setFechaExpiracion(Util.convertToDate(tarjetaRequest.getFechaVencimiento()));
                }

                listaTarjetaEntity.add(tarjeta);
            }

            listaTarjetaEntity = tarjetaEntityRepository.saveAll(listaTarjetaEntity);

            for (TarjetaEntity tarjetaEntity : listaTarjetaEntity) {
                clienteTarjetaEntityRepository.insertClienteTarjeta(identificador, tarjetaEntity.getIdTarjeta());
                clienteTarjetaEntityRepository.insertSaldo(identificador, tarjetaEntity.getIdTarjeta(), 0.0);
            }
        } catch (Exception e){
            System.out.println("Error: "+e);
        }

        return identificador;
    }

    @Override
    @Transactional
    public RecargaTarjetaResponse recargarTarjeta(RecargaTarjetaRequest request) throws Exception {

        RecargaTarjetaResponse response = new RecargaTarjetaResponse();
        Audit audit = new Audit();
        audit.setMensajeRespuesta("Recarga exitosa");
        audit.setCodigoRespuesta(0);

        String fechaTransaccion = Util.convertToString(new Date());
        Integer numeroTransaccion = (int) (1000000000 * Math.random());

        try {
            List<TarjetaEntityDto> listaTarjetas = tarjetaRepository.buscarTarjetasAsociadas(request.getIdentificadorCliente());
            Integer idTarjeta = null;
            Double saldo = 0.0;

            for (TarjetaEntityDto tarjetaEntityDto : listaTarjetas ) {
                if(tarjetaEntityDto.getNROTARJETA().equals(request.getNumeroTarjetaTransporte())){
                    saldo = tarjetaEntityDto.getMONTO();
                    idTarjeta = tarjetaEntityDto.getIDTARJETA();
                }
            }

            saldo += request.getMontoRecarga();

            clienteTarjetaEntityRepository.registrarMovimiento(request, idTarjeta, numeroTransaccion);
            clienteTarjetaEntityRepository.actualizarSaldo(request.getIdentificadorCliente(), idTarjeta, saldo);
            response.setNumeroTransaccion(String.valueOf(numeroTransaccion));
            response.setFechaTransaccion(fechaTransaccion);

            //Twilio.init(ACCOUNT_SID, AUTH_TOKEN);

            //Message message = Message.creator(new PhoneNumber("+15005550006"),new PhoneNumber("+51959704778"),"Mensaje de prueba").create();

            //System.out.println(message.getSid());

        } catch (Exception e){
            System.out.println("Error: "+e);
            audit.setMensajeRespuesta("Error interno");
            audit.setCodigoRespuesta(1);
        }

        envioCorreo.sendPago(request, fechaTransaccion, numeroTransaccion);

        response.setAudit(audit);

        return response;
    }

    @Override
    public LoginResponse validarUsuario(LoginRequest request) throws Exception {

        LoginResponse response = new LoginResponse();
        Audit audit = new Audit();
        audit.setCodigoRespuesta(0);
        audit.setMensajeRespuesta("Consulta exitosa");

        ClienteEntity cliente = clienteEntityRepository.findByCorreoAndContrasenaAndEstado(request.getUsuario(), request.getClave(), 'A');

        if(cliente == null){
            audit.setCodigoRespuesta(1);
            audit.setMensajeRespuesta("Usuario o clave incorrecto");
            response.setAudit(audit);
            return response;
        }
        response.setAudit(audit);
        response.setCorreo(cliente.getCorreo());
        response.setIdentificador(cliente.getIdCliente());
        response.setNombre(cliente.getNombres());
        response.setFechaRegistro(Util.convertToString(cliente.getFechaRegistro()));
        return response;
    }

    @Override
    public RegistrarUsuarioResponse registrarUsuario(RegistrarUsuarioRequest request) throws Exception {

        RegistrarUsuarioResponse response = new RegistrarUsuarioResponse();
        Audit audit = new Audit();
        audit.setCodigoRespuesta(0);
        audit.setMensajeRespuesta("Registro exitoso");

        ClienteEntity cliente = clienteEntityRepository.findByCorreo(request.getCorreo());

        if(cliente!=null){
            audit.setCodigoRespuesta(1);
            audit.setMensajeRespuesta("El correo ya se encuentra registrado");
            response.setAudit(audit);
            return response;
        }

        ClienteEntity clienteNuevo = new ClienteEntity();
        clienteNuevo.setEstado('A');
        clienteNuevo.setFechaRegistro(new Date());
        clienteNuevo.setContrasena(request.getClave());
        clienteNuevo.setCorreo(request.getCorreo());
        clienteNuevo.setDni(String.valueOf(request.getDni()));
        clienteNuevo.setNombres(request.getUsuario());
        clienteNuevo.setNroCelular(String.valueOf(request.getNumeroCelular()));

        clienteNuevo = clienteEntityRepository.save(clienteNuevo);

        response.setAudit(audit);
        response.setIdentificador(clienteNuevo.getIdCliente());

        return response;
    }

    @Override
    public ActualizarClaveResponse actualizarClave(ActualizarClaveRequest request) throws Exception {

        ActualizarClaveResponse response = new ActualizarClaveResponse();
        Audit audit = new Audit();
        audit.setCodigoRespuesta(0);
        audit.setMensajeRespuesta("Actualizacion exitosa");

        ClienteEntity cliente = clienteEntityRepository.findByIdCliente(request.getIdentificador());

        if(cliente == null){
            audit.setCodigoRespuesta(1);
            audit.setMensajeRespuesta("No existe usuario");
            response.setAudit(audit);
            return response;
        }

        if(cliente.getContrasena().equals(request.getClaveAntigua())){
            cliente.setContrasena(request.getClaveNueva());
            clienteEntityRepository.save(cliente);
        }else{
            audit.setCodigoRespuesta(2);
            audit.setMensajeRespuesta("Clave antigua no es correcta");
        }
        response.setAudit(audit);

        return response;
    }

    @Override
    public RecuperarContraseñaResponse recuperarClave(RecuperarContraseñaRequest request) throws Exception {

        RecuperarContraseñaResponse response = new RecuperarContraseñaResponse();
        Audit audit = new Audit();
        audit.setCodigoRespuesta(0);
        audit.setMensajeRespuesta("Correo enviado exitosamente");
        try {
            ClienteEntity cliente = clienteEntityRepository.findByCorreo(request.getCorreo());
            if(cliente == null){
                audit.setCodigoRespuesta(1);
                audit.setMensajeRespuesta("No existe usuario registrado");
                response.setAudit(audit);
                return response;
            }

            envioCorreo.sendRecuperarClave(cliente.getCorreo(), cliente.getNombres(), cliente.getContrasena());

        } catch (Exception e){
            audit.setCodigoRespuesta(2);
            audit.setMensajeRespuesta("Error interno");
        }

        response.setAudit(audit);

        return response;
    }

    private ListarTarjetaResponse getListaTarjeta(List<TarjetaEntityDto> lista){

        Audit audit = new Audit();
        ListarTarjetaResponse response = new ListarTarjetaResponse();
        List<TarjetaAsociada> listaTarjetaAsociada = new ArrayList<>();

        Integer codigo = 0;
        String mensaje = "";

        if(lista.isEmpty()){
            codigo = 1;
            mensaje = "No se encontraron resultado";
        }else{
            mensaje = "Consulta Existosa";

            for (TarjetaEntityDto tarjetaEntityDto: lista) {
                TarjetaAsociada tarjetaAsociada = new TarjetaAsociada();
                tarjetaAsociada.setIdTarjeta(tarjetaEntityDto.getIDTARJETA());
                tarjetaAsociada.setNumeroTarjeta(tarjetaEntityDto.getNROTARJETA());
                tarjetaAsociada.setEstadoTarjeta(EstadoEnum.valueOf(tarjetaEntityDto.getESTADO()).getEstadoDescripcion());
                tarjetaAsociada.setTipoServicio(tarjetaEntityDto.getDESCRIPCION());
                tarjetaAsociada.setSaldo(tarjetaEntityDto.getMONTO());
                tarjetaAsociada.setFechaVencimiento(tarjetaEntityDto.getFECHAEXPIRACION());

                listaTarjetaAsociada.add(tarjetaAsociada);
            }
            response.setListaTarjetas(listaTarjetaAsociada);
        }

        audit.setCodigoRespuesta(codigo);
        audit.setMensajeRespuesta(mensaje);

        response.setAudit(audit);

        return response;

    }
}
