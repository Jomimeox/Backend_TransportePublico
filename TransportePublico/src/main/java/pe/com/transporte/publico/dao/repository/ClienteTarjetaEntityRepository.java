package pe.com.transporte.publico.dao.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import pe.com.transporte.publico.dao.entity.ClienteTarjetaEntity;
import pe.com.transporte.publico.dto.RecargaTarjetaRequest;

@Repository
public interface ClienteTarjetaEntityRepository extends JpaRepository<ClienteTarjetaEntity, Long> {

    @Modifying
    @Query(value = "INSERT INTO cliente_tarjeta (IdCliente, IdTarjeta, FechaRegistro, Estado) VALUES ( :idCliente, :idTarjeta, now(), 'A')",
            nativeQuery = true)
    void insertClienteTarjeta(@Param("idCliente") Integer idCliente, @Param("idTarjeta") Integer idTarjeta);

    @Modifying
    @Query(value = "INSERT INTO SALDO (IdCliente, IdTarjeta, Monto) VALUES (:idCliente, :idTarjeta, :monto)",
            nativeQuery = true)
    void insertSaldo(@Param("idCliente") Integer idCliente, @Param("idTarjeta") Integer idTarjeta, @Param("monto") Double monto);

    @Modifying
    @Query(value = "INSERT INTO detalle_movimiento (IdCliente, IdTarjeta, NroTransaccion, Monto, IndicadorMovimiento, FechaRegistro, Estado) " +
            "VALUES (:#{#recarga.identificadorCliente}, :idTarjeta, :numeroTransaccion, :#{#recarga.montoRecarga}, 'R', now(), 'A')",
            nativeQuery = true)
    void registrarMovimiento(@Param("recarga") RecargaTarjetaRequest recarga, @Param("idTarjeta") Integer idTarjeta, @Param("numeroTransaccion") Integer numeroTransaccion);

    @Modifying
    @Query(value = "UPDATE SALDO SET MONTO = :monto WHERE IdCliente = :idCliente AND IdTarjeta = :idTarjeta",
            nativeQuery = true)
    void actualizarSaldo(@Param("idCliente") Integer idCliente, @Param("idTarjeta") Integer idTarjeta, @Param("monto") Double monto);

}
