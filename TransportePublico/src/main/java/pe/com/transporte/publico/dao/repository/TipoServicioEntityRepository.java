package pe.com.transporte.publico.dao.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import pe.com.transporte.publico.dao.entity.TiposervicioEntity;

import java.util.List;

@Repository
public interface TipoServicioEntityRepository extends JpaRepository<TiposervicioEntity, Integer> {

    List<TiposervicioEntity> findByEstadoEquals(Character estado);

    @Query(value = "SELECT IDTIPOSERVICIO, DESCRIPCION,ESTADO FROM TIPOSERVICIO WHERE ESTADO = 'A'",
            nativeQuery = true)
    List<TiposervicioEntity> obtenerServicios();
}
