package pe.com.transporte.publico.common;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum EstadoEnum {

    A("A","Activo"),
    I("I","Inactivo");

    private String estadoCode;
    private String estadoDescripcion;
}
