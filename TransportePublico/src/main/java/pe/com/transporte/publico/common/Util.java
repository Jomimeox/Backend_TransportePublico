package pe.com.transporte.publico.common;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Util {

    public static Date convertToDate(String fechaCadena) throws Exception{
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            return sdf.parse(fechaCadena);
        }catch (Exception e){
            throw new Exception(e.getMessage());
        }
    }

    public static String convertToString(Date date) throws Exception{
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:MM:ss");
            return sdf.format(date);
        }catch (Exception e){
            throw new Exception(e.getMessage());
        }
    }
}
